package com.gitlab.demetron11.cryptotelegrambot.network;

import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiService {

    @Headers("Content-Type: application/json")
    @GET("assets")
    Call<Coins> getCoins(@Query("api_key") String apiKey, @Query("limit") Integer limit);

    @Headers("Content-Type: application/json")
    @GET("assets")
    Call<Coins> getCoin(@Query("api_key") String apiKey, @Query("search") String search, @Query("limit") Integer limit);
}
