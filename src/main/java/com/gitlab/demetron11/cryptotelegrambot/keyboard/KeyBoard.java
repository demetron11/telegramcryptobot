package com.gitlab.demetron11.cryptotelegrambot.keyboard;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyBoard {
    public static final Map<BotState, ReplyKeyboardMarkup> STATE_KEY_BOARD = new HashMap<BotState, ReplyKeyboardMarkup>();
    public static final ReplyKeyboardMarkup BEGIN = getBegin();
    public static final ReplyKeyboardMarkup MY_ACCOUNT = getMyAccount();

    private static ReplyKeyboardMarkup getBegin() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow row1 = new KeyboardRow();
        row1.add(new KeyboardButton("Все валюты \uD83D\uDCB5"));
        row1.add(new KeyboardButton("Мои валюты \uD83D\uDCBC"));

        KeyboardRow row2 = new KeyboardRow();
        row2.add(new KeyboardButton("Настройки ⚙️"));

        keyboard.add(row1);
        keyboard.add(row2);

        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    private static ReplyKeyboardMarkup getMyAccount() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow row1 = new KeyboardRow();
        row1.add(new KeyboardButton("Добавить валюту \uD83D\uDC81"));
        row1.add(new KeyboardButton("Удалить валюту \uD83D\uDE45"));
        KeyboardRow row2 = new KeyboardRow();
        row2.add(new KeyboardButton("Курс в реальном времени \uD83D\uDD54"));
        row2.add(new KeyboardButton("Настройка уведомлений \uD83D\uDD14"));
        KeyboardRow row3 = new KeyboardRow();
        row3.add(new KeyboardButton("Назад \uD83D\uDC48"));
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);


        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    static {
        STATE_KEY_BOARD.put(BotState.BEGIN, BEGIN);
        STATE_KEY_BOARD.put(BotState.MY_ACCOUNT, MY_ACCOUNT);

    }

}
