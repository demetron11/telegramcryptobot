package com.gitlab.demetron11.cryptotelegrambot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CryptoTelegramBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(CryptoTelegramBotApplication.class, args);
    }

}
