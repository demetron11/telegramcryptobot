package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import com.gitlab.demetron11.cryptotelegrambot.network.ApiFactory;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CryptoHandler implements Handler {

    private final UserService userService;

    public CryptoHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {
        Response<Coins> response = null;

        User user = userService.getUser(update.getMessage().getChatId());
        if (user == null) {
            userService.createUser(update.getMessage().getChatId(), update.getMessage().getChat().getFirstName(), false);
        }
        List<String> currencies = userService.getCurrenciesByUser(update.getMessage().getChatId());

        if ((currencies.isEmpty() && (update.getMessage().getText().equals("Мои валюты \uD83D\uDCBC")
                || update.getMessage().getText().equals("Удалить валюту \uD83D\uDE45")))) {
            return SendMessage.builder()
                    .chatId(update.getMessage().getChatId().toString())
                    .text("Сейчас у Вас не добавлено ни одной валюты. Вы можете сделать это в меню \"Настройки\"")
                    .build();
        }

        List<List<InlineKeyboardButton>> buttons;
        if (user != null && ((!currencies.isEmpty() &&
                botState == BotState.BEGIN) || botState == BotState.DELETE_CURRENCY) && !update.getMessage().getText().equals("Все валюты \uD83D\uDCB5")) {

            AtomicInteger i = new AtomicInteger();
            buttons = new ArrayList<>(currencies.stream()
                    .map(currency -> {
                        InlineKeyboardButton button = new InlineKeyboardButton(currency);
                        button.setCallbackData(currency);
                        return button;
                    }).collect(Collectors.groupingBy(x -> i.getAndIncrement() / 4))
                    .values());
        } else {
            try {
                do {
                    Call<Coins> call = ApiFactory.getService().getCoins("87a49394-0efa-466d-97a5-58853bf7ca1d", 20);
                    response = call.execute();
                } while (response.body() == null);
            } catch (IOException e) {
                log.error(e.getMessage());
            }

            AtomicInteger i = new AtomicInteger();
            buttons = new ArrayList<>(response.body().getCoins().stream()
                    .map(coin -> {
                        InlineKeyboardButton button = new InlineKeyboardButton(coin.getName());
                        button.setCallbackData(coin.getName());
                        return button;
                    })
                    .collect(Collectors.groupingBy(x -> i.getAndIncrement() / 4))
                    .values());
        }

        StringBuilder stringBuilder = new StringBuilder();

        if (botState == BotState.DELETE_CURRENCY) {
            stringBuilder.append("Осторожно! Удаление валюты также удалит и текущие подписки на отслеживание курса этой валюты: \n\n");
            InlineKeyboardButton button = new InlineKeyboardButton("Удалить всё");
            button.setCallbackData("Удалить всё");
            buttons.add(Collections.singletonList(button));
        } else {
            stringBuilder.append("Список валют: \n\n");
        }
        if (botState == BotState.ADD_CURRENCY) {
            InlineKeyboardButton button = new InlineKeyboardButton("Добавить всё");
            button.setCallbackData("Добавить всё");
            buttons.add(Collections.singletonList(button));
        }
        InlineKeyboardMarkup markupKeyboard = new InlineKeyboardMarkup();
        markupKeyboard.setKeyboard(buttons);
        return SendMessage.builder().chatId(update.getMessage().getChatId().toString())
                .text(stringBuilder.toString())
                .replyMarkup(markupKeyboard)
                .build();
    }
}
