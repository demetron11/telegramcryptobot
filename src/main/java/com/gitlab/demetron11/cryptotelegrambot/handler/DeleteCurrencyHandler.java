package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class DeleteCurrencyHandler implements Handler {

    private final UserService userService;

    public DeleteCurrencyHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {
        if (update.getCallbackQuery().getData().equals("Удалить всё")) {
            userService.deleteAllCurrencies(update.getCallbackQuery().getMessage().getChatId());
            return SendMessage.builder()
                    .chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                    .text("Все валюты удалены")
                    .build();
        }

        userService.deleteCurrency(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getData());
        return SendMessage.builder()
                .chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                .text("У Вас больше нет власти над " + update.getCallbackQuery().getData())
                .build();
    }
}
