package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class AddCurrencyHandler implements Handler {

    private final UserService userService;

    public AddCurrencyHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {
        if (update.getCallbackQuery().getData().equals("Добавить всё")) {
            userService.addAllCurrencies(update.getCallbackQuery().getMessage().getChatId());
            return SendMessage.builder()
                    .chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                    .text("Вы собрали их все! Теперь у Вас абсолютная власть!")
                    .build();
        }

        if (userService.addCurrency(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getData())) {
            return SendMessage.builder()
                    .chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                    .text("Вы получили власть над " + update.getCallbackQuery().getData())
                    .build();
        } else {
            return SendMessage.builder()
                    .chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                    .text("У Вас уже есть власть над " + update.getCallbackQuery().getData())
                    .build();
        }
    }
}
