package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.exchange.ExchangeService;
import com.gitlab.demetron11.cryptotelegrambot.keyboard.KeyBoard;
import com.gitlab.demetron11.cryptotelegrambot.model.Coin;
import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import com.gitlab.demetron11.cryptotelegrambot.network.ApiFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@Component
@Slf4j
public class CryptoDetailHandler implements Handler {

    private final ExchangeService exchangeService;

    public CryptoDetailHandler(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {

        Coin coin = null;
        try {
            coin = getCoin(update);
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return SendMessage.builder().chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                .text(coin.getName() + " сейчас стоит " + coin.getPriceUsd().substring(0, coin.getPriceUsd().indexOf('.') + 3) + " USD" +
                        " / " + exchangeService.getRubRate(coin.getPriceUsd()) + " RUB")
                .replyMarkup(KeyBoard.STATE_KEY_BOARD.get(botState))
                .build();
    }

    private Coin getCoin(Update update) throws IOException {
        Response<Coins> response;
        do {
            Call<Coins> call = ApiFactory.getService().getCoin("87a49394-0efa-466d-97a5-58853bf7ca1d", update.getCallbackQuery().getData(), 1);
            response = call.execute();
        } while (response.body() == null);
        Coin coin = response.body().getCoins().get(0);
        return coin;
    }
}
