package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class SubscribeHandler implements Handler {
    private final UserService userService;

    public SubscribeHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {
        boolean isSubscribed = userService.setSubscribe(update.getMessage().getChatId());
        String message;
        if (isSubscribed) {
            message = "Оповещения включены. Теперь каждые 15 секунд мы будем присылать Вам котировки выбранных валют";
        } else {
            message = "Оповещения выключены";
        }
        return SendMessage.builder().chatId(update.getMessage().getChatId().toString()).text(message).build();
    }
}
