package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.keyboard.KeyBoard;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class MyAccountHandler implements Handler {

    private final UserService userService;

    public MyAccountHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {

        userService.createUser(update.getMessage().getChatId(), update.getMessage().getChat().getFirstName(), false);

        return SendMessage.builder()
                .chatId(update.getMessage().getChatId().toString())
                .text("Приветствую,  " + update.getMessage().getChat().getFirstName() + "!!")
                .replyMarkup(KeyBoard.MY_ACCOUNT).build();
    }
}
