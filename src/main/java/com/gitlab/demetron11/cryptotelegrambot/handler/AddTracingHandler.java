package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import com.gitlab.demetron11.cryptotelegrambot.network.ApiFactory;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.TracingValueService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@Component
@Slf4j
public class AddTracingHandler implements Handler {
    @Setter
    private String currencyName;

    private final TracingValueService tracingValueService;

    public AddTracingHandler(TracingValueService tracingValueService) {
        this.tracingValueService = tracingValueService;
    }

    @Override
    public SendMessage handle(Update update, BotState botState) {

        String text = update.getMessage().getText();
        Long chatId = update.getMessage().getChatId();

        if (!text.matches("(-?\\d+\\.?\\d?%?\\s*)+(1)*")) {
            return SendMessage.builder().chatId(chatId.toString())
                    .text("Вы ввели некорректные значения")
                    .build();
        }

        double currencyPrice = 0;
        try {
            currencyPrice = getCurrencyPrice();
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        if (!parse(text, chatId, currencyPrice)) return SendMessage.builder().chatId(chatId.toString())
                .text("Вы ввели некорректные значения")
                .build();

        return SendMessage.builder()
                .chatId(chatId.toString())
                .text(currencyName + " следит за Вами")
                .build();
    }

    private boolean parse(String text, Long chatId, double currencyPrice) {
        String[] stringValues = text.split(" ");
        try {
            for (String stringValue : stringValues) {
                double value;
                String percent;
                if (stringValue.matches(".*%+")) {
                    percent = stringValue.split("%")[0];
                    value = currencyPrice * (1 + Double.parseDouble(percent) / 100);
                } else {
                    value = Double.parseDouble(stringValue);
                    percent = "concrete";
                }
                if (value <= 0) return true;
                tracingValueService.addTracingValue(chatId, currencyName, value, percent);
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private double getCurrencyPrice() throws IOException {
        Response<Coins> response;
        do {
            Call<Coins> call = ApiFactory.getService().getCoin("87a49394-0efa-466d-97a5-58853bf7ca1d", currencyName, 1);
            response = call.execute();
        } while (response.body() == null);

        return Double.parseDouble(response.body().getCoins().get(0).getPriceUsd());
    }
}
