package com.gitlab.demetron11.cryptotelegrambot.handler;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class GetTracingHandler implements Handler {
    @Override
    public SendMessage handle(Update update, BotState botState) {
        return SendMessage.builder()
                .chatId(update.getCallbackQuery().getMessage().getChatId().toString())
                .text("Введите список отслеживаемых значений для " + update.getCallbackQuery().getData() +
                        "\n\nКогда курс " + update.getCallbackQuery().getData() + " достигнет одной из заданных величин," +
                        " Вам придет оповещение\n\nЗначения должны быть списком дробных величин или процентов, например:" +
                        " \n\n10% -5% 40000 43000.5 -15.6%")
                .build();
    }
}
