package com.gitlab.demetron11.cryptotelegrambot.botstate;

public enum BotState {
    START,
    BEGIN,
    MY_ACCOUNT,
    ADD_CURRENCY,
    DELETE_CURRENCY,
    GET_TRACING,
    ADD_TRACING,
    ALERT
}
