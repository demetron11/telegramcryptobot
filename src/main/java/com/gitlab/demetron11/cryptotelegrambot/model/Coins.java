package com.gitlab.demetron11.cryptotelegrambot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
@Data
public class Coins {

    @SerializedName("data")
    @Expose
    private List<Coin> coins = null;
    @SerializedName("timestamp")
    @Expose
    private Long timestamp;

}