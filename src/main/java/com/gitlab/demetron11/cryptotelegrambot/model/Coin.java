package com.gitlab.demetron11.cryptotelegrambot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
@Data
public class Coin {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("supply")
    @Expose
    private String supply;
    @SerializedName("maxSupply")
    @Expose
    private Object maxSupply;
    @SerializedName("marketCapUsd")
    @Expose
    private String marketCapUsd;
    @SerializedName("volumeUsd24Hr")
    @Expose
    private String volumeUsd24Hr;
    @SerializedName("priceUsd")
    @Expose
    private String priceUsd;
    @SerializedName("changePercent24Hr")
    @Expose
    private String changePercent24Hr;
    @SerializedName("vwap24Hr")
    @Expose
    private String vwap24Hr;
    @SerializedName("explorer")
    @Expose
    private String explorer;

}
