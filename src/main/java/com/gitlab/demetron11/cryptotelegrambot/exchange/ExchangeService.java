package com.gitlab.demetron11.cryptotelegrambot.exchange;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
@Slf4j
public class ExchangeService {

    private Double rate;

    public ExchangeService() {
        setRate();
    }

    public String getRubRate(String priceUsd) {
        return String.format("%.2f", Double.parseDouble(priceUsd) * rate);
    }

    @Scheduled(cron = "0 0 * * * ?")
    private void setRate() {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readTree(new URL("https://www.cbr-xml-daily.ru/latest.js"));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        rate = 1 / jsonNode.get("rates").get("USD").asDouble();
    }

}
