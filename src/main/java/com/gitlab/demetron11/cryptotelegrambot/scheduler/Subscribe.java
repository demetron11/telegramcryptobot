package com.gitlab.demetron11.cryptotelegrambot.scheduler;

import com.gitlab.demetron11.cryptotelegrambot.config.CryptoBot;
import com.gitlab.demetron11.cryptotelegrambot.exchange.ExchangeService;
import com.gitlab.demetron11.cryptotelegrambot.model.Coin;
import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import com.gitlab.demetron11.cryptotelegrambot.network.ApiFactory;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class Subscribe {

    private final CryptoBot cryptoBot;
    private final UserService userService;
    private final ExchangeService exchangeService;
    @Getter
    private Map<Long, Message> messages = new HashMap<>();

    public Subscribe(@Lazy CryptoBot cryptoBot, UserService userService, ExchangeService exchangeService) {
        this.cryptoBot = cryptoBot;
        this.userService = userService;
        this.exchangeService = exchangeService;
    }


    @Scheduled(cron = "${scheduler.subscribe}")
    @SneakyThrows
    public void sendToSubscribers() {

        List<User> isSubscribedTrue = userService.findByIsSubscribedIsTrue();

        Map<String, String> currencyAvgPrice = getCurrencyAvgPrice();

        for (User user : isSubscribedTrue) {

            List<String> currencies = userService.getCurrenciesByUser(user.getId());

            if (!currencies.isEmpty()) {

                StringBuilder stringBuilder = new StringBuilder();

                for (String currency : currencies) {

                    stringBuilder.append(currency).append(" сейчас стоит ").append(currencyAvgPrice.get(currency))
                            .append(" USD / ").append(exchangeService.getRubRate(currencyAvgPrice.get(currency)))
                            .append(" RUB").append("\n");
                }
                if (messages.get(user.getId()) != null) {

                    try {

                        cryptoBot.execute(EditMessageText.builder()
                                .chatId(user.getId().toString())
                                .replyMarkup(messages.get(user.getId()).getReplyMarkup())
                                .messageId(messages.get(user.getId()).getMessageId())
                                .text(stringBuilder.toString())
                                .build());
                    } catch (TelegramApiException e) {
                        //иногда выскакивает 400 ошибка, почему - непонятно, кажется, что особенности telegram API
                        //ничему не мешает
                    }
                } else {
                    messages.put(user.getId(), cryptoBot.execute(SendMessage.builder().chatId(user.getId().toString())
                            .text(stringBuilder.toString())
                            .build()));
                }
            }

        }

    }

    private Map<String, String> getCurrencyAvgPrice() throws IOException {
        Response<Coins> response;
        do {
            Call<Coins> call = ApiFactory.getService().getCoins("87a49394-0efa-466d-97a5-58853bf7ca1d", 20);
            response = call.execute();
        } while (response.body() == null);
        return response.body().getCoins().stream()
                .collect(Collectors.toMap(Coin::getName, coin -> coin.getPriceUsd().substring(0, coin.getPriceUsd().indexOf(".") + 3)));
    }

    @SneakyThrows
    public void deleteMessage(Long id) {
        if (messages.get(id) != null) {
            cryptoBot.execute(DeleteMessage.builder()
                    .chatId(messages.get(id).getChatId().toString())
                    .messageId(messages.get(id).getMessageId())
                    .build());
            messages.computeIfPresent(id, (aLong, message) -> null);
        }
    }
}
