package com.gitlab.demetron11.cryptotelegrambot.scheduler;

import com.gitlab.demetron11.cryptotelegrambot.config.CryptoBot;
import com.gitlab.demetron11.cryptotelegrambot.model.Coin;
import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import com.gitlab.demetron11.cryptotelegrambot.network.ApiFactory;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.TracingValue;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.CurrencyService;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.TracingValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CheckValue {

    private final CurrencyService currencyService;
    private final TracingValueService tracingValueService;
    private final CryptoBot cryptoBot;

    public CheckValue(CurrencyService currencyService, TracingValueService tracingValueService, CryptoBot cryptoBot) {
        this.currencyService = currencyService;
        this.tracingValueService = tracingValueService;
        this.cryptoBot = cryptoBot;
    }

    @Scheduled(cron = "${scheduler.check}")
    public void checkValue() {
        List<Currency> allCurrencies = currencyService.getAllCurrencies();

        try {
            setNewValues(allCurrencies, getCoinNames());
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        Map<String, Currency> currencyMap = allCurrencies.stream().collect(Collectors.toMap(Currency::getId, Function.identity()));

        List<TracingValue> allValues = tracingValueService.getAllValues();

        for (TracingValue value : allValues) {

            Currency currency = currencyMap.get(value.getCurrency().getId());

            if (isChecked(value.getValue(), currency.getOldValue(), currency.getNewValue())) {

                String chatId = value.getUsers().getId().toString();

                try {
                    if (value.getPercent() == null || value.getPercent().equals("concrete")) {
                        cryptoBot.execute(SendMessage.builder()
                                .chatId(chatId)
                                .text(value.getCurrency().getId() + " достиг значения "
                                        + String.format("%.2f", value.getValue()) + " USD" + "\n").build());

                    } else {

                        double percentDouble = Double.parseDouble(value.getPercent());
                        if (percentDouble > 0) cryptoBot.execute(SendMessage.builder()
                                .chatId(chatId)
                                .text(value.getCurrency().getId() + " вырос на "
                                        + String.format("%.2f", percentDouble) + "%" + "\n").build());

                        else cryptoBot.execute(SendMessage.builder()
                                .chatId(chatId)
                                .text(value.getCurrency().getId() + " упал на "
                                        + String.format("%.2f", percentDouble) + "%" + "\n").build());

                        tracingValueService.delete(value.getId());
                    }
                } catch (TelegramApiException e) {
                    log.error(e.getMessage());
                }

            }
        }


    }

    private void setNewValues(List<Currency> allCurrencies, Map<String, String> coinNames) {
        for (Currency currency : allCurrencies) {
            currency.setOldValue(currency.getNewValue());
            if (coinNames.containsKey(currency.getId())) {
                currency.setNewValue(Double.parseDouble(coinNames.get(currency.getId())));
            }
        }
        currencyService.saveAllCurrencies(allCurrencies);
    }

    private Map<String, String> getCoinNames() throws IOException {
        Response<Coins> response;
        do {
            Call<Coins> call = ApiFactory.getService().getCoins("87a49394-0efa-466d-97a5-58853bf7ca1d", 20);
            response = call.execute();
        } while (response.body() == null);
        return response.body().getCoins().stream().collect(Collectors.toMap(Coin::getName, Coin::getPriceUsd));
    }

    private boolean isChecked(Double value, Double oldValue, Double newValue) {
        return value > oldValue ? value < newValue : value > newValue;
    }
}
