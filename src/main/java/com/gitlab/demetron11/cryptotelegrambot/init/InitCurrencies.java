package com.gitlab.demetron11.cryptotelegrambot.init;

import com.gitlab.demetron11.cryptotelegrambot.model.Coins;
import com.gitlab.demetron11.cryptotelegrambot.network.ApiFactory;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.CurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@Component
@Slf4j
public class InitCurrencies {

    private final CurrencyService currencyService;

    public InitCurrencies(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Response<Coins> response;
        try {

            do {
                Call<Coins> call = ApiFactory.getService().getCoins("87a49394-0efa-466d-97a5-58853bf7ca1d", 20);
                response = call.execute();
            } while (response.body() == null);

            response.body().getCoins().stream()
                    .forEach(coin -> currencyService.create(new Currency(coin.getName(), coin.getSymbol(),
                            Double.parseDouble(coin.getPriceUsd()), Double.parseDouble(coin.getPriceUsd()))));

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
