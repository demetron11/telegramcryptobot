package com.gitlab.demetron11.cryptotelegrambot.config;

import com.gitlab.demetron11.cryptotelegrambot.facade.CryptoFacade;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
@Getter
@Slf4j
public class CryptoBot extends TelegramWebhookBot {

    private final CryptoFacade cryptoFacade;

    @Value("${telegrambot.name}")
    private String botUsername;

    @Value("${telegrambot.token}")
    private String botToken;

    @Value("${telegrambot.url}")
    private String botPath;

    public CryptoBot(CryptoFacade cryptoFacade) {
        this.cryptoFacade = cryptoFacade;
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
        if (update.hasMessage()) {
            log.info(String.format("Sent %s from %s", update.getMessage().getText(), update.getMessage().getChat().getFirstName()));
        }
        return cryptoFacade.handle(update);
    }

    @PostConstruct
    public void init() {
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(this, SetWebhook.builder().url(this.botPath).build());
            setWebHook();
        } catch (TelegramApiException | IOException e) {
            log.error(e.getMessage());
        }
    }

    public void setWebHook() throws IOException {
        String url = String.format("https://api.telegram.org/bot%s/setWebhook?url=%s", getBotToken(), getBotPath());
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        log.info(connection.getResponseMessage());
    }
}