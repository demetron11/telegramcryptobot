package com.gitlab.demetron11.cryptotelegrambot.usersdata.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "currency", schema = "public")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Currency {
    @Id
    @Column(name = "name", nullable = false, length = 50)
    private String id;

    @Column(name = "symbol", length = 10)
    private String symbol;

    @Column
    private Double oldValue;

    @Column
    private Double newValue;

}