package com.gitlab.demetron11.cryptotelegrambot.usersdata.repository;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByIsSubscribedIsTrue();


}