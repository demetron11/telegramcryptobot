package com.gitlab.demetron11.cryptotelegrambot.usersdata.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "users_currency")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TracingValue {

    public TracingValue(User users, Currency currency, Double value, String percent) {
        this.users = users;
        this.currency = currency;
        this.value = value;
        this.percent = percent;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "users")
    private User users;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency")
    private Currency currency;

    @Column(name = "values", nullable = false)
    private Double value;

    @Column(name = "percent")
    private String percent;

}