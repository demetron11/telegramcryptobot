package com.gitlab.demetron11.cryptotelegrambot.usersdata.service;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;

import java.util.List;

public interface CurrencyService {
    void create(Currency currency);

    List<Currency> getAllCurrencies();

    void saveAllCurrencies(List<Currency> currencies);
}
