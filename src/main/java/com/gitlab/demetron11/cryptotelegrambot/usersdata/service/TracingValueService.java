package com.gitlab.demetron11.cryptotelegrambot.usersdata.service;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.TracingValue;

import java.util.List;

public interface TracingValueService {
    void addTracingValue(Long chatId, String currencyName, Double val, String percent);

    List<TracingValue> getAllValues();

    void delete(Long id);
}
