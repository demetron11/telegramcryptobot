package com.gitlab.demetron11.cryptotelegrambot.usersdata.repository;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, String> {
}