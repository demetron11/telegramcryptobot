package com.gitlab.demetron11.cryptotelegrambot.usersdata.repository;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.TracingValue;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TracingValueRepository extends JpaRepository<TracingValue, Long> {
    boolean existsByUsersAndCurrency(User user, Currency currency);

    @Query(value = "SELECT DISTINCT currency FROM users_currency WHERE users= :id", nativeQuery = true)
    List<String> findDistinctByUsers_Id(@Param("id") Long id);

    void deleteByUsers_IdAndCurrency_Id(Long id, String currencyName);

    void deleteByUsers_Id(Long id);

}