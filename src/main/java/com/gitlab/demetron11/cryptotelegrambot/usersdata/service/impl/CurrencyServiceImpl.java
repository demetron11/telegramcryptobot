package com.gitlab.demetron11.cryptotelegrambot.usersdata.service.impl;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.CurrencyRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.CurrencyService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public void create(Currency currency) {
        currencyRepository.save(currency);
    }

    @Override
    public List<Currency> getAllCurrencies() {
        return currencyRepository.findAll();
    }

    @Override
    public void saveAllCurrencies(List<Currency> currencies) {
        currencyRepository.saveAll(currencies);
    }
}
