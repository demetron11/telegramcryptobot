package com.gitlab.demetron11.cryptotelegrambot.usersdata.service.impl;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.TracingValue;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.CurrencyRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.TracingValueRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.UserRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private CurrencyRepository currencyRepository;
    private TracingValueRepository tracingValueRepository;


    @Override
    public boolean addCurrency(Long id, String currencyName) {
        User user = userRepository.findById(id).orElse(null);
        Currency currency = currencyRepository.getById(currencyName);
        if (tracingValueRepository.existsByUsersAndCurrency(user, currency)) {
            return false;
        } else {
            tracingValueRepository.save(new TracingValue(user, currency, 0.0, "concrete"));
            return true;
        }
    }

    @Override
    public boolean createUser(Long id, String name, boolean isSubscribe) {
        if (!userRepository.existsById(id)) {
            userRepository.save(new User(id, name, isSubscribe));
        }
        return true;
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void deleteCurrency(Long id, String currencyName) {
        tracingValueRepository.deleteByUsers_IdAndCurrency_Id(id, currencyName);
    }

    @Override
    public List<User> findByIsSubscribedIsTrue() {
        return userRepository.findByIsSubscribedIsTrue();
    }

    @Override
    public boolean setSubscribe(Long id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            if (user.isSubscribed()) {
                user.setSubscribed(false);
                userRepository.save(user);
                return false;
            } else {
                user.setSubscribed(true);
                userRepository.save(user);
                return true;
            }

        }
        return false;
    }

    @Override
    public List<String> getCurrenciesByUser(Long id) {
        return tracingValueRepository.findDistinctByUsers_Id(id);
    }

    @Override
    public void addAllCurrencies(Long id) {
        User user = userRepository.findById(id).orElse(null);
        List<Currency> currencies = currencyRepository.findAll();
        for (Currency currency : currencies) {
            if (!tracingValueRepository.existsByUsersAndCurrency(user, currency)) {
                tracingValueRepository.save(new TracingValue(user, currency, 0.0, "concrete"));
            }
        }
    }

    @Override
    @Transactional
    public void deleteAllCurrencies(Long id) {
        tracingValueRepository.deleteByUsers_Id(id);
    }


}
