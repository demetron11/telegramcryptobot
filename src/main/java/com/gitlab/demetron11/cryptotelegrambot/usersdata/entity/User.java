package com.gitlab.demetron11.cryptotelegrambot.usersdata.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "users", schema = "public")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "chatid", nullable = false)
    private Long id;

    @Column(name = "name", length = 50)
    private String name;

    @Column
    private boolean isSubscribed;

}