package com.gitlab.demetron11.cryptotelegrambot.usersdata.service.impl;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.Currency;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.TracingValue;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.CurrencyRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.TracingValueRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.repository.UserRepository;
import com.gitlab.demetron11.cryptotelegrambot.usersdata.service.TracingValueService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TracingValueServiceImpl implements TracingValueService {

    private final TracingValueRepository tracingValueRepository;
    private final UserRepository userRepository;
    private final CurrencyRepository currencyRepository;

    public TracingValueServiceImpl(TracingValueRepository tracingValueRepository, UserRepository userRepository, CurrencyRepository currencyRepository) {
        this.tracingValueRepository = tracingValueRepository;
        this.userRepository = userRepository;
        this.currencyRepository = currencyRepository;
    }

    @Override
    public void addTracingValue(Long chatId, String currencyName, Double val, String percent) {
        User user = userRepository.getById(chatId);
        Currency currency = currencyRepository.getById(currencyName);
        tracingValueRepository.save(new TracingValue(user, currency, val, percent));
    }

    @Override
    public List<TracingValue> getAllValues() {
        return tracingValueRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        TracingValue valueRepositoryById = tracingValueRepository.getById(id);
        tracingValueRepository.delete(valueRepositoryById);
    }
}
