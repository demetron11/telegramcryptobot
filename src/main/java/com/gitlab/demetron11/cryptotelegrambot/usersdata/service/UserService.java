package com.gitlab.demetron11.cryptotelegrambot.usersdata.service;

import com.gitlab.demetron11.cryptotelegrambot.usersdata.entity.User;

import java.util.List;

public interface UserService {
    boolean addCurrency(Long id, String currencyName);

    boolean createUser(Long id, String name, boolean isSubscribe);

    User getUser(Long id);

    void deleteCurrency(Long chatId, String data);

    List<User> findByIsSubscribedIsTrue();

    boolean setSubscribe(Long id);

    List<String> getCurrenciesByUser(Long id);

    void addAllCurrencies(Long id);

    void deleteAllCurrencies(Long id);
}
