package com.gitlab.demetron11.cryptotelegrambot.facade;

import com.gitlab.demetron11.cryptotelegrambot.botstate.BotState;
import com.gitlab.demetron11.cryptotelegrambot.handler.*;
import com.gitlab.demetron11.cryptotelegrambot.keyboard.KeyBoard;
import com.gitlab.demetron11.cryptotelegrambot.scheduler.Subscribe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class CryptoFacade {

    private final MyAccountHandler myAccountHandler;
    private final AddCurrencyHandler addCurrencyHandler;
    private final CryptoHandler cryptoHandler;
    private final DeleteCurrencyHandler deleteCurrencyHandler;
    private final SubscribeHandler subscribeHandler;
    private final AddTracingHandler addTracingHandler;
    private final GetTracingHandler getTracingHandler;
    private final CryptoDetailHandler cryptoDetailHandler;
    private final Subscribe subscribe;

    private Map<Long, BotState> botContext = new HashMap<>();

    public CryptoFacade(MyAccountHandler myAccountHandler, AddCurrencyHandler addCurrencyHandler,
                        CryptoHandler cryptoHandler, DeleteCurrencyHandler deleteCurrencyHandler, SubscribeHandler subscribeHandler,
                        AddTracingHandler addTracingHandler, GetTracingHandler getTracingHandler,
                        CryptoDetailHandler cryptoDetailHandler, Subscribe subscribe) {
        this.myAccountHandler = myAccountHandler;
        this.addCurrencyHandler = addCurrencyHandler;
        this.cryptoHandler = cryptoHandler;
        this.deleteCurrencyHandler = deleteCurrencyHandler;
        this.subscribeHandler = subscribeHandler;
        this.addTracingHandler = addTracingHandler;
        this.getTracingHandler = getTracingHandler;
        this.cryptoDetailHandler = cryptoDetailHandler;
        this.subscribe = subscribe;
    }

    public BotApiMethod<?> handle(Update update) {

        if (update.hasCallbackQuery()) {

            log.info(String.format("Sent callback %s from %s", update.getCallbackQuery().getData(), update.getCallbackQuery().getMessage().getChat().getFirstName()));

            subscribe.deleteMessage(update.getCallbackQuery().getMessage().getChatId());

            botContext.putIfAbsent(update.getCallbackQuery().getMessage().getChatId(), BotState.BEGIN);

            Long chatId = update.getCallbackQuery().getMessage().getChatId();
            if (botContext.get(chatId) == BotState.BEGIN || botContext.get(chatId) == BotState.MY_ACCOUNT) {
                return cryptoDetailHandler.handle(update, botContext.get(chatId));
            }
            if (botContext.get(chatId) == BotState.ADD_CURRENCY) {
                return addCurrencyHandler.handle(update, botContext.get(chatId));
            }
            if (botContext.get(chatId) == BotState.DELETE_CURRENCY) {
                return deleteCurrencyHandler.handle(update, BotState.DELETE_CURRENCY);
            }
            if (botContext.get(chatId) == BotState.GET_TRACING) {
                SendMessage message = getTracingHandler.handle(update, BotState.GET_TRACING);
                addTracingHandler.setCurrencyName(update.getCallbackQuery().getData());
                botContext.put(chatId, BotState.ADD_TRACING);
                return message;
            }
            return null;
        }

        if (update.hasMessage()) {

            subscribe.deleteMessage(update.getMessage().getChatId());

            botContext.putIfAbsent(update.getMessage().getChatId(), BotState.BEGIN);

            if (update.getMessage().hasText()) {
                String textMessage = update.getMessage().getText();
                Long chatId = update.getMessage().getChatId();
                if (textMessage.equals("/start")) {
                    return SendMessage.builder()
                            .chatId(update.getMessage().getChatId().toString())
                            .text("Приветствую!!\n\nЯ Ваш личный помощник в торговле криптовалютой")
                            .build();
                }
                if (textMessage.equals("Все валюты \uD83D\uDCB5")) {
                    botContext.put(chatId, BotState.BEGIN);
                    return cryptoHandler.handle(update, botContext.get(chatId));
                }
                if (textMessage.equals("Курс в реальном времени \uD83D\uDD54")) {
                    botContext.put(chatId, BotState.MY_ACCOUNT);
                    return subscribeHandler.handle(update, botContext.get(chatId));
                }
                if (textMessage.equals("Мои валюты \uD83D\uDCBC")) {
                    botContext.put(chatId, BotState.BEGIN);
                    return cryptoHandler.handle(update, botContext.get(chatId));
                }
                if (textMessage.equals("Добавить валюту \uD83D\uDC81")) {
                    botContext.put(chatId, BotState.ADD_CURRENCY);
                    return cryptoHandler.handle(update, botContext.get(chatId));
                }
                if (textMessage.equals("Удалить валюту \uD83D\uDE45")) {
                    botContext.put(chatId, BotState.DELETE_CURRENCY);
                    return cryptoHandler.handle(update, botContext.get(chatId));
                }
                if (textMessage.equals("Настройка уведомлений \uD83D\uDD14")) {
                    botContext.put(chatId, BotState.GET_TRACING);
                    return cryptoHandler.handle(update, botContext.get(chatId));
                }
                if (textMessage.equals("Настройки ⚙️")) {
                    botContext.put(chatId, BotState.MY_ACCOUNT);
                    return myAccountHandler.handle(update, botContext.get(chatId));
                }
                if (botContext.getOrDefault(chatId, BotState.BEGIN) == BotState.ADD_TRACING) {
                    SendMessage message = addTracingHandler.handle(update, botContext.get(chatId));
                    botContext.put(chatId, BotState.GET_TRACING);
                    return message;
                }
                botContext.put(chatId, BotState.BEGIN);
                return SendMessage.builder().chatId(chatId.toString())
                        .text("В нашем боте Вы можете узнать курс криптовалют.\n\nВ меню \"Настройки\" Вам доступно создание удобной" +
                                " клавиатуры со своим списком валют.\n\nТам же Вы можете включить постоянную отправку курса валют в чат " +
                                "или настроить оповещения о достижении выбранными криптовалютами определенных величин")
                        .replyMarkup(KeyBoard.BEGIN).build();
            }
        }
        return null;
    }
}
