# BUILD STAGE
FROM eclipse-temurin:11-alpine AS builder
COPY . /src
WORKDIR /src
RUN ./mvnw clean package 

# RUNTIME STAGE
FROM eclipse-temurin:11-alpine as bot
WORKDIR /app
COPY --from=builder /src/target/*.jar ./app.jar
EXPOSE 8088
